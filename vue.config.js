module.exports = {
  devServer: {
    proxy: {
      "^/api": {
        target: "https://www.xdfznh.club/atoolsbe",
        pathRewrite: {
          "^/api": "",
        },
        changeOrigin: true,
      },
    },
  },
  configureWebpack: {
    resolve: {
      alias: {
        components: "@/components",
      },
    },
  },
};
