import { App } from "vue";
import * as Icons from "@element-plus/icons-vue";

function registerElementPlusIcon(app: App) {
  Object.keys(Icons).forEach((key) => {
    app.component(key, (Icons as any)[key]);
  });
}

export { registerElementPlusIcon };
