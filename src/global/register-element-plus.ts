import { App } from "vue";
import "element-plus/theme-chalk/index.css"; // 全局引用样式 - 有通过 babel 按需引用样式的方法

import { ElButton, ElIcon, ElTable, ElTableColumn } from "element-plus";
const elementPlusComponents = [ElButton, ElIcon, ElTable, ElTableColumn];

function registerElementPlus(app: App) {
  elementPlusComponents.forEach((cmpt) => {
    app.component(cmpt.name, cmpt);
  });
}

export { registerElementPlus };
