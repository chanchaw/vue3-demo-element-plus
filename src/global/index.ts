import { registerElementPlus } from "./register-element-plus";
import { registerElementPlusIcon } from "./register-element-icon";

export { registerElementPlus, registerElementPlusIcon };
