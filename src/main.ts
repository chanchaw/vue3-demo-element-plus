import { createApp } from "vue";
import App from "./App.vue";

import { registerElementPlus, registerElementPlusIcon } from "@/global";
import { router } from "@/router";

const app = createApp(App);

registerElementPlus(app); // 按需引用 element-plus 组件
registerElementPlusIcon(app); // 按需引用 icon
app.use(router);

app.mount("#app");
