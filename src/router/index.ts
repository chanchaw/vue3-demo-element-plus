import { createRouter, createWebHashHistory } from "vue-router";
import Table from "@/components/table.vue";
import Hello from "@/components/HelloWorld.vue";

const routes = [
  { path: "/table", component: Table },
  { path: "/hello", component: Hello },
];

const router = createRouter({
  routes,
  history: createWebHashHistory(),
});

export { router };
